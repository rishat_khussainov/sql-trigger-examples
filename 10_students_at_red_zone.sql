DROP FUNCTION if exists get_students_at_red_zone();

create function get_students_at_red_zone()
returns table (student_id integer, name character varying(50), surname character varying(50))
language plpgsql
as
$$
begin
   return query select s.id as student_id, s.name, s.surname from students s 
   join (select e.student_id from exam_results e
   where e.mark <= 3
   group by e.student_id
   having count(*) > 1) red_zone_students on red_zone_students.student_id = id;
end;
$$;