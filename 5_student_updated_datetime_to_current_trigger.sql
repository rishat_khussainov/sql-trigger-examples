DROP TRIGGER IF EXISTS TR_Student_Update ON students; 
DROP FUNCTION IF EXISTS update_student_updated_datetime();

CREATE FUNCTION update_student_updated_datetime()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
	NEW.updated_datetime := now();
    RETURN new;
END;
$$;

CREATE TRIGGER TR_Student_Update
BEFORE UPDATE ON students
FOR EACH ROW
EXECUTE PROCEDURE update_student_updated_datetime();