CREATE TABLE IF NOT EXISTS subjects (
	id serial PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	tutor VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS students (
	id serial PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	surname VARCHAR(50),
	date_of_birth date NOT NULL,
	phone_number VARCHAR(50) NOT NULL,	
	primary_skill_id bigint NOT NULL,
	created_datetime timestamp NOT NULL,
	updated_datetime timestamp,
	FOREIGN KEY (primary_skill_id) REFERENCES subjects(id)
);

CREATE TABLE IF NOT EXISTS exam_results (
	id serial PRIMARY KEY,
	student_id bigint NOT NULL,
	subject_id bigint NOT NULL,
	mark SMALLINT NOT NULL,
	FOREIGN KEY (student_id) REFERENCES students(id),
	FOREIGN KEY (subject_id) REFERENCES subjects(id)	
);

CREATE TABLE IF NOT EXISTS student_address (
	id serial PRIMARY KEY,
	address VARCHAR(100) NOT NULL,
	city VARCHAR(50) NOT NULL,
	country VARCHAR(50) NOT NULL,
	postal_code VARCHAR(50) NOT NULL,
	student_id bigint NOT NULL,
	FOREIGN KEY (student_id) REFERENCES students(id)
);

CREATE TABLE IF NOT EXISTS student_address_new (
	id serial PRIMARY KEY,
	address VARCHAR(100) NOT NULL,
	city VARCHAR(50) NOT NULL,
	country VARCHAR(50) NOT NULL,
	postal_code VARCHAR(50) NOT NULL,
	student_id bigint NOT NULL,
	FOREIGN KEY (student_id) REFERENCES students(id)
);