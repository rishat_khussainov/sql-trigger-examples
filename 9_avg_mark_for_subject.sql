DROP FUNCTION if exists get_avg_mark_by_subject(varchar);

create function get_avg_mark_by_subject(p_subject_name varchar)
returns numeric
language plpgsql
as
$$
declare
   avg_mark numeric;
begin
   select avg(mark) 
   into avg_mark
   from exam_results e
   join subjects s on s.id = e.subject_id 
   where s.name = p_subject_name;
   return avg_mark;
end;
$$;