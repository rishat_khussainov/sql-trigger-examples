DROP FUNCTION if exists get_avg_mark_by_student_id(integer);

create function get_avg_mark_by_student_id(p_student_id integer)
returns numeric
language plpgsql
as
$$
declare
   avg_mark numeric;
begin
   select avg(mark) 
   into avg_mark
   from exam_results
   where student_id = p_student_id;   
   return avg_mark;
end;
$$;