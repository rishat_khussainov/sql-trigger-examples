DROP TRIGGER IF EXISTS TR_Student_Address_Insert_Update ON student_address; 
DROP FUNCTION IF EXISTS update_student_address();

CREATE FUNCTION update_student_address()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
	IF TG_OP = 'INSERT' THEN
    	insert into student_address_new(address, city, country, postal_code, student_id)
		values (NEW.address, NEW.city, NEW.country, NEW.postal_code, NEW.student_id);
    	RAISE INFO 'The value was inserted into student_address_new table instead of inserting new';
    ELSIF TG_OP = 'UPDATE' THEN
        insert into student_address_new(address, city, country, postal_code, student_id)
		values (COALESCE(NEW.address, OLD.address), COALESCE(NEW.city, OLD.city),
		   COALESCE(NEW.country, OLD.country), COALESCE(NEW.postal_code, OLD.postal_code),
		   COALESCE(NEW.student_id, OLD.student_id));
    	RAISE INFO 'The value was inserted into student_address_new table instead of updating old';
    END IF;
    RETURN null;
END;
$$;

CREATE TRIGGER TR_Student_Address_Insert_Update
BEFORE INSERT OR UPDATE ON student_address
FOR EACH ROW
EXECUTE PROCEDURE update_student_address();