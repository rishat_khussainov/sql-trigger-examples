ALTER SEQUENCE exam_results_id_seq RESTART WITH 1;
ALTER SEQUENCE students_id_seq RESTART WITH 1;

truncate table exam_results CASCADE;
truncate table students CASCADE;

ALTER SEQUENCE subjects_id_seq RESTART WITH 1;
truncate table subjects CASCADE;

create or replace function generate_random_string() 
returns text 
language sql as $$
  SELECT string_agg (substr('abcdefghijklmnopqrstuvwxyz', ceil (random() * 62)::integer, 1), '')
  FROM generate_series(1, 15)
$$;


insert into subjects (
    name, tutor
)
select
    generate_random_string(),
	generate_random_string()
from generate_series(1, 1000) s(i);


insert into students (
	name, surname, date_of_birth, phone_number, primary_skill_id, created_datetime
)
select
	generate_random_string(),
	generate_random_string(),
	now(),
	trunc(random() * 9000000000 + 1)::text,
	trunc(random() * 1000 + 1),
	now()
from generate_series(1, 100000) s(i);


insert into exam_results (
    student_id, subject_id, mark
)
select
    trunc(random() * 100000 + 1),
	trunc(random() * 10 + 1),
	trunc(random() * 10 + 1)
from generate_series(1, 1000000) s(i);