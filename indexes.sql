CREATE INDEX hash_idx_students_name ON students USING hash (name);
CREATE INDEX hash_idx_students_surname ON students USING hash (surname);
CREATE INDEX hash_idx_students_phone_number ON students USING hash (phone_number);
CREATE INDEX hash_idx_subjects_name ON subjects USING hash(name);