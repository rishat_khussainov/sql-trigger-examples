create materialized view student_snapshot
 as
 select s.name, s.surname, sub.name as subject, e.mark
 from exam_results as e
 join students as s ON s.id = e.student_id 
 join subjects as sub ON sub.id = e.subject_id;